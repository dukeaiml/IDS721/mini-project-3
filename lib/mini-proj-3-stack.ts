import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as iam from 'aws-cdk-lib/aws-iam';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

export class MiniProj3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    
    // make an S3 bucket that enables versioning and encrption
    const bucket = new cdk.aws_s3.Bucket(this, 'Hathy-MiniProj3', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.KMS_MANAGED,
      enforceSSL: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY
    });
    
    bucket.grantRead(new iam.AccountRootPrincipal());
  }
}
